# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Simple Rest API Backend Integration

### How do I get set up? ###

Clone The Project Into IDE
Run maven clean and install
Maven update projet
Run as spring boot app
Access the api's from postman
http://localhost:8080/app/api/addPerson ---POST, Request body in json
http://localhost:8080/app/api/updatePerson  ---POST, Request body in json
http://localhost:8080/app/api/fetchAllPerson  --GET
http://localhost:8080/app/api/fetchPersonById?id=1  --GET, Request Param Id
http://localhost:8080/app/api/deletePersonById?id=1  --GET, Request Param Id


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact