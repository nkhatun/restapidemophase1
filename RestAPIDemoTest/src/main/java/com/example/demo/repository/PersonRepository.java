package com.example.demo.repository;

import com.example.demo.domain.Person;
import com.example.demo.dto.ResponseDTO;

public interface PersonRepository {
	public ResponseDTO addPerson(Person p);
	public ResponseDTO updatePerson(Person p);
	public ResponseDTO deletePerson(int id);
	public Person getPerson(int id);
	public Person[] getAllPersons();
}
