package com.example.demo.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class ResponseDTO implements Serializable{
	private boolean status;
	private String message;
    List elements = new ArrayList<>();
    
    public ResponseDTO() {
		super();
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List getElements() {
		return elements;
	}
	public void setElements(List elements) {
		this.elements = elements;
	}
}
