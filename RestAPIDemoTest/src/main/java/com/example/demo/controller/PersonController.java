package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.domain.Person;
import com.example.demo.dto.ResponseDTO;
import com.example.demo.repository.PersonRepository;
import com.example.demo.service.PersonService;

@RequestMapping("/app/api")
@RestController
public class PersonController {
	
	@Autowired
	private PersonService personService;
	
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value = "/addPerson", method = RequestMethod.POST, consumes = "application/json")
	public ResponseDTO addPerson(@RequestBody Person personDTO) {
		ResponseDTO responseDTO = personService.addPerson(personDTO);
		return responseDTO;
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value = "/updatePerson", method = RequestMethod.POST, consumes = "application/json")
	public ResponseDTO updatePerson(@RequestBody Person personDTO) {
		ResponseDTO responseDTO = personService.updatePerson(personDTO);
		return responseDTO;
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value = "/fetchAllPerson", method = RequestMethod.GET)
	public ResponseDTO fetchAllPerson() {
		ResponseDTO responseDTO = personService.getAllPersons();
		return responseDTO;
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value = "/fetchPersonById", method = RequestMethod.GET)
	public ResponseDTO fetchPersonById(@RequestParam int id) {
		ResponseDTO responseDTO = personService.getPerson(id);
		return responseDTO;
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value = "/deletePersonById", method = RequestMethod.GET)
	public ResponseDTO deletePersonById(@RequestParam int id) {
		ResponseDTO responseDTO = personService.deletePerson(id);
		return responseDTO;
	}

}
