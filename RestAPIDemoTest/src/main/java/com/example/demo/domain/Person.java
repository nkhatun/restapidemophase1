package com.example.demo.domain;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Person {
	private int id;
	private String first_name;
	private String last_name;
	private int age;
	private String favourite_colour;
	private List<String> hobby;
	
/*	public Person(@JsonProperty("first_name") String first_name,
			@JsonProperty("first_name") String last_name,
			@JsonProperty("age") int age,
			@JsonProperty("favourite_colour") String favourite_colour,
			@JsonProperty("hobby") List<String> hobby) {
		super();
		//this.id = id;
		this.first_name = first_name;
		this.last_name = last_name;
		this.age = age;
		this.favourite_colour = favourite_colour;
		this.hobby = hobby;
	}*/
	
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public List<String> getHobby() {
		return hobby;
	}
	public void setHobby(List<String> hobby) {
		this.hobby = hobby;
	}
	public String getFavourite_colour() {
		return favourite_colour;
	}
	public void setFavourite_colour(String favourite_colour) {
		this.favourite_colour = favourite_colour;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}
