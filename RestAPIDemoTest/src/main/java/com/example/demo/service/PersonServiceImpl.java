package com.example.demo.service;

import java.util.Arrays;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.domain.Person;
import com.example.demo.dto.ResponseDTO;
import com.example.demo.repository.PersonRepository;

@Service
public class PersonServiceImpl implements PersonService{
	@Autowired
	private PersonRepository personRepository;
	
	@Override
	public ResponseDTO addPerson(Person p) {
		return personRepository.addPerson(p);
	}

	@Override
	public ResponseDTO deletePerson(int id) {
		return personRepository.deletePerson(id);
	}

	@Override
	public ResponseDTO getPerson(int id) {
		ResponseDTO responseDTO = new ResponseDTO();
		Person person = personRepository.getPerson(id);
		if(person != null) {
			responseDTO.setElements(Collections.singletonList(person));
			responseDTO.setMessage("Person Details Fetched Successfully");
			responseDTO.setStatus(true);
		}
		else {
			responseDTO.setMessage("No Person Details Found");
			responseDTO.setStatus(true);
		}
		return responseDTO;
	}

	@Override
	public ResponseDTO getAllPersons() {
		ResponseDTO responseDTO = new ResponseDTO();
		Person[] persons = personRepository.getAllPersons();
		if(persons != null && persons.length > 0) {
			responseDTO.setElements(Arrays.asList(persons));
			responseDTO.setMessage("Person List Fetched Successfully");
			responseDTO.setStatus(true);
		}
		else {
			responseDTO.setMessage("No Person Registered Yet");
			responseDTO.setStatus(true);
		}
		return responseDTO;
	}

	@Override
	public ResponseDTO updatePerson(Person p) {
		return personRepository.updatePerson(p);
	}

}
