package com.example.demo.service;

import com.example.demo.domain.Person;
import com.example.demo.dto.ResponseDTO;

public interface PersonService {
	public ResponseDTO addPerson(Person p);
	public ResponseDTO updatePerson(Person p);
	public ResponseDTO deletePerson(int id);
	public ResponseDTO getPerson(int id);
	public ResponseDTO getAllPersons();
}
